import React from 'react';
import pictoTest from './style/images/pictoTest.png'
import pictoTest2 from './style/images/pictoTest2.jpeg'
import img1 from './style/images/1.jpg'
import img2 from './style/images/2.jpg'
import img3 from './style/images/3.jpg'
import img4 from './style/images/4.jpg'
import img5 from './style/images/5.jpg'
import img6 from './style/images/6.jpg'
import img7 from './style/images/7.jpg'
import img8 from './style/images/8.jpg'
import img9 from './style/images/9.jpg'
import img10 from './style/images/10.jpg'
import img11 from './style/images/11.jpg'
import img12 from './style/images/12.jpg'
import img13 from './style/images/13.jpg'
import img14 from './style/images/14.jpg'
import img15 from './style/images/15.jpg'
import img16 from './style/images/16.jpg'
import img17 from './style/images/17.jpg'
import img18 from './style/images/18.jpg'
import img19 from './style/images/19.jpg'
import img20 from './style/images/20.jpg'



import axios from 'axios';

class AffichePartitionProgressif extends React.Component {

    state = {
        currentPartition: [],
    };

    imgDispatcher(nomNote) {
        switch (nomNote) {
            case "pictoTest": return pictoTest;
            case "pictoTest2": return pictoTest2;
            case "img1": return img1;
            case "img2": return img2;
            case "img3": return img3;
            case "img4": return img4;
            case "img5": return img5;
            case "img6": return img6;
            case "img7": return img7;
            case "img8": return img8;
            case "img9": return img9;
            case "img10": return img10;
            case "img11": return img11;
            case "img12": return img12;
            case "img13": return img13;
            case "img14": return img14;
            case "img15": return img15;
            case "img16": return img16;
            case "img17": return img17;
            case "img18": return img18;
            case "img19": return img19;
            case "img20": return img20;


        }
    }

    componentDidMount() {

        const nomPartition = "Nom_Partition_2";
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        axios.get('http://localhost:5000/api/partitions/get1partition/' + nomPartition, config.headers)
            .then(res => {
                this.state.currentPartition = res.data.notes;
            });
    }


    render() {
        let tabImg = [];
        tabImg = this.props.image.map(img => {
            return this.imgDispatcher(img);
        }
        );
        const imagePartition = tabImg.map(image =>
            <span className="AffichePartitonProgressif">
                <img src={image} width='250' height='250' />
            </span>
        )
        return (
            <div>
                {imagePartition}
            </div>
        );
    }
}

export default AffichePartitionProgressif;
