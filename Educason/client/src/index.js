import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import { Piano, KeyboardShortcuts, MidiNumbers } from 'react-piano';
import 'react-piano/dist/styles.css';
import DimensionsProvider from './DimensionsProvider';
import AffichePartition from './AffichePartition';
import AffichePartitionProgressif from './AffichePartitionProgressif';

import SoundfontProvider from './SoundfontProvider';
import PianoWithRecording from './PianoWithRecording';
import PartitionList from './PartitionList';
import axios from 'axios';
import ComboSelect from 'react-combo-select';
require('../node_modules/react-combo-select/style.css');


// webkitAudioContext fallback needed to support Safari
const audioContext = new (window.AudioContext || window.webkitAudioContext)();
const soundfontHostname = 'https://d1pzp51pvbm36p.cloudfront.net';

const noteRange = {
  first: MidiNumbers.fromNote('A0'),
  last: MidiNumbers.fromNote('C8'),
};
const keyboardShortcuts = KeyboardShortcuts.create({
  firstNote: noteRange.first,
  lastNote: noteRange.last,
  keyboardConfig: KeyboardShortcuts.EDUCASON_ROW,
});

const config = {
  headers: {
    'Content-Type': 'application/json'
  }
}
let list = [];
let startMid;


class App extends React.Component {
  state = {
    recording: {
      mode: 'RECORDING',
      events: [],
      currentTime: 0,
      currentEvents: [],
    },
    currentPartition: [],
    currentIndex: 0,
    image: "",
    play: false,
    tabImage: [],
    nbImg: 5,
    tab: []

  };

  constructor(props) {
    super(props);

    this.scheduledEvents = [];
  }

  componentWillMount() {
    this.freeImage();
  }
  componentDidMount() {
    this.getImage(this.state.currentIndex)
  }

  getImgClick = async (index) => {
    const nomPartition = "Nom_Partition_2";
    let ind;

    this.state.tabImage = [];
    this.state.tab = [];

    if( (index + this.state.nbImg) < this.state.currentPartition.length){
      ind = index + this.state.nbImg;
    }
    else
      ind = this.state.currentPartition.length;

      console.log("ind :" +ind);

    for (var i = index; i < ind ; i++) {
      const resFor = await axios.get('http://localhost:5000/api/notes/get1Note/' + this.state.currentPartition[i].midiNumber, config.headers)
      console.log("resFor.data")
      console.log(resFor.data)
      resFor.data.map(data =>
        this.state.tab.push(data.url)
        );
    } 
    console.log("tab 2 eme then");
    console.log(this.state.tab);

    console.log("tab 3 eme then");
    console.log(this.state.tab);
    this.setState({ tabImage: this.state.tab })
  }
  getImage = async (index) => {
    const nomPartition = "Nom_Partition_2";
    let tabStartMid = [];
    let ind;
    this.state.currentIndex = 0;
    const res = await axios.get('http://localhost:5000/api/partitions/get1partition/' + nomPartition, config.headers)
    this.state.currentPartition = res.data.notes;

    if( (index + this.state.nbImg) < this.state.currentPartition.length){
      ind = index + this.state.nbImg;
    }
    else
      ind = this.state.currentPartition.length;

      console.log("ind :" +ind);

    for (var i = index; i < ind ; i++) {
      const resFor = await axios.get('http://localhost:5000/api/notes/get1Note/' + this.state.currentPartition[i].midiNumber, config.headers)
      console.log(resFor.data)
      resFor.data.map(data =>
        this.state.tab.push(data.url)
        );
    } 
    console.log("tab 2 eme then");
    console.log(this.state.tab);

    console.log("tab 3 eme then");
    console.log(this.state.tab);
    this.setState({ tabImage: this.state.tab })

    // axios.get('http://localhost:5000/api/partitions/get1partition/' + nomPartition, config.headers)
    //   .then(res => {
    //     this.state.currentPartition = res.data.notes;
    //   })
    //   .then(res => {
    //     for (var i = 0; i < this.state.nbImg; i++) {
    //       axios.get('http://localhost:5000/api/notes/get1Note/' + this.state.currentPartition[i].midiNumber, config.headers)
    //       .then(res => {
    //         res.data.map(data =>
    //           this.state.tab.push(data.url)
    //           );
    //       });
    //     } 
    //     console.log("tab 2 eme then");
    //     console.log(this.state.tab);
    //   })
    //   .then(res => {
    //     console.log("tab 3 eme then");
    //     console.log(this.state.tab);
    //     this.setState({ tabImage: this.state.tab })
    //   })
    //   ;
  }

  freeImage = () => {
    this.setState({tabImage: []});
  }

  getRecordingEndTime = () => {
    if (this.state.recording.events.length === 0) {
      return 0;
    }
    return Math.max(
      ...this.state.recording.events.map(event => event.time + event.duration),
    );
  };

  setRecording = value => {
    this.setState({
      recording: Object.assign({}, this.state.recording, value),
    });
  };

  onClickPlay = () => {
    this.setRecording({
      mode: 'PLAYING',
    });
    const startAndEndTimes = _.uniq(
      _.flatMap(this.state.recording.events, event => [
        event.time,
        event.time + event.duration,
      ]),
    );
    startAndEndTimes.forEach(time => {
      this.scheduledEvents.push(
        setTimeout(() => {
          const currentEvents = this.state.recording.events.filter(event => {
            return event.time <= time && event.time + event.duration > time;
          });
          this.setRecording({
            currentEvents,
          });
        }, time * 1000),
      );
    });
    // Stop at the end
    setTimeout(() => {
      this.onClickStop();
    }, this.getRecordingEndTime() * 1000);
  };


  onClickSave = () => {

    //const body = JSON.stringify(test);
    var partition = {
      name: "Nom_Partition_3",
      notes: []
    };

    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    const startAndEndTimes = _.uniq(
      _.flatMap(this.state.recording.events, event => [
        event.time,
        event.time + event.duration,
      ]),
    );
    var i = 0;
    this.state.recording.events.forEach(note => {
      partition.notes.push(note);
    });
    axios.post('http://localhost:5000/api/partitions/savePartition', partition, config.headers)
      .then(res => {
        // console.log(res);
        console.log(res.data);
        console.log("Ajout ok")
      })
      .catch(error => {
        console.log(error.response.data);
      });
  };

  onClickStop = () => {
    this.scheduledEvents.forEach(scheduledEvent => {
      clearTimeout(scheduledEvent);
    });
    this.setRecording({
      mode: 'RECORDING',
      currentEvents: [],
    });
  };

  onClickClear = () => {
    this.onClickStop();
    this.setRecording({
      events: [],
      mode: 'RECORDING',
      currentEvents: [],
      currentTime: 0,
    });
  };

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  verifNotes = () => {
    let eventMid, currentMid, nexMid;
    this.state.recording.events.map(event =>
      eventMid = event.midiNumber
    );

    this.state.recording.events.map(event =>
      currentMid = this.state.currentPartition[this.state.currentIndex].midiNumber
    );

    if (this.state.currentIndex != this.state.currentPartition.length - 1) {

      this.state.recording.events.map(event =>
        nexMid = this.state.currentPartition[this.state.currentIndex + 1].midiNumber
      );
    }

    if ((eventMid == currentMid) && this.state.play == true) {
      let rdm = this.getRandomInt(2);
      if (rdm == 1) {
        console.log("Bien jouer !!!");
        console.log("Touche jouer");
        console.log(this.state.recording.events[this.state.recording.events.length - 1]);
        console.log("Note partition");
        console.log(this.state.recording.events[this.state.recording.events.length - 1]);
      }
      this.state.currentIndex = this.state.currentIndex + 1;
      if (this.state.currentIndex == this.state.currentPartition.length) {
        alert("BRAVOOOOOO !!!!!")
      }

      axios.get('http://localhost:5000/api/notes/get1Note/' + nexMid, config.headers)
        .then(res => {
          res.data.map(data =>
            this.setState({ image: data.url })
          );
        });

    }
  };

  verifModeProgressif = () => {
    let eventMid, currentMid, nexMid;
    let startMid;
    let tab = [];
    this.state.recording.events.map(event =>
      eventMid = event.midiNumber
    );
    this.state.recording.events.map(event =>
      currentMid = this.state.currentPartition[this.state.currentIndex].midiNumber
    );
    if (this.state.currentIndex != this.state.currentPartition.length - 1) {
      this.state.recording.events.map(event =>
        nexMid = this.state.currentPartition[this.state.currentIndex + 1].midiNumber
      );
    }
    if ((eventMid == currentMid) && this.state.play == true) {
      let rdm = this.getRandomInt(2);
      if (rdm == 1) {
        alert("BIEN JOUER !!!")
        console.log("Bien jouer !!!");
        console.log("Touche jouer");
        console.log(this.state.recording.events[this.state.recording.events.length - 1]);
        console.log("Note partition");
        console.log(this.state.recording.events[this.state.recording.events.length - 1]);
      }
      this.state.currentIndex = this.state.currentIndex + 1;
      if (this.state.currentIndex == this.state.currentPartition.length) {
        alert("BRAVOOOOOO FIN DE LA PARTIE!!!!!")
      }

      //this.setState({tabImage: []})
      this.getImgClick(this.state.currentIndex);
      //this.componentDidMount();
    }
  };


  startGame = () => {
    this.state.currentIndex = 0;
    this.state.play = true;

    //console.log("tab");
    //console.log(tab);
    //this.setState({ tabImage: tab})

    /*affichage sequentiel
    axios.get('http://localhost:5000/api/notes/get1Note/' + startMid, config.headers)
      .then(res => {
        res.data.map(data =>
          this.setState({ image: data.url })
        );
      });
      */
  }

  ResponsivePiano(props) {
    return (
      <DimensionsProvider>
        <myComponent />
        {({ containerWidth, containerHeight }) => (
          <SoundfontProvider
            instrumentName="acoustic_grand_piano"
            audioContext={audioContext}
            hostname={soundfontHostname}
            render={({ isLoading, playNote, stopNote }) => (
              <PianoWithRecording
                recording={this.state.recording}
                setRecording={this.setRecording}
                noteRange={noteRange}
                width={containerWidth}
                playNote={playNote}
                stopNote={stopNote}
                disabled={isLoading}
                {...props}
              />
            )}
          />
        )}
      </DimensionsProvider>
    );
  }

  getListPartitions() {
    var getJistPartition = [];
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    axios.get('http://localhost:5000/api/partitions/getList/name', config.headers)
      .then(data => { return data.json(); })
      .then(res => {
        // console.log(res);
        const data = JSON.stringify(res.data);
        getJistPartition = data;
        getJistPartition = getJistPartition.split(",");
      })
    return getJistPartition;
  };

  render() {
    return (
      <div> 
        <AffichePartitionProgressif
          image={this.state.tabImage}
        />

        <div className="mt-5">
          <DimensionsProvider>
            {({ containerWidth, containerHeight }) => (
              <SoundfontProvider
                instrumentName="acoustic_grand_piano"
                audioContext={audioContext}
                hostname={soundfontHostname}
                render={({ isLoading, playNote, stopNote }) => (
                  <PianoWithRecording
                    onClick={this.verifModeProgressif()}
                    recording={this.state.recording}
                    setRecording={this.setRecording}
                    noteRange={noteRange}
                    width={containerWidth}
                    playNote={playNote}
                    stopNote={stopNote}
                    disabled={isLoading}
                    keyboardShortcuts={keyboardShortcuts}
                  />
                )}
              />
            )}
          </DimensionsProvider>
        </div>
        <PartitionList />
        <div className="mt-5">
          <button onClick={this.onClickPlay}>Play</button>
          <button onClick={this.onClickSave}>Save</button>
          <button onClick={this.onClickStop}>Stop</button>
          <button onClick={this.onClickClear}>Clear</button>
          <button onClick={this.startGame}>Start Game/Replay</button>
        </div>
        <div className="mt-5">
          <strong>Recorded notes</strong>
          <div>{JSON.stringify(this.state.recording.events)}</div>
        </div>
      </div>
    );
  }
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
