import React from 'react';
import axios from 'axios';

export default class PartitionList extends React.Component {
state = {
    partitions: [],
};

componentDidMount() {
    axios.get('http://localhost:5000/api/partitions/getList/name').then(res => {
        this.setState({partitions: res.data});
    })
}

render(){
    return <select >{this.state.partitions.map(partition => <option> {partition.name}</option>)}
    </select>;
}

}
