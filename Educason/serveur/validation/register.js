const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';

  if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = 'Votre nom doit être compris entre 2 et 30 caractères.';
  }

  if (Validator.isEmpty(data.name)) {
    errors.name = 'Votre nom est obligatoire.';
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = 'Votre Email est obligatoire.';
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = 'Votre Email est invalide.';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Votre mot de passe est obligatoire.';
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Votre mot de passe doit être compris entre 6 et 30 caractères.';
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = 'La confirmation de votre mot de passe est obligatoire.';
  }

  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = 'Vos mots de passe ne correspondent pas.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
