const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLoginInput(data) {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';

  if (!Validator.isEmail(data.email)) {
    errors.email = 'Votre Email est invalide.';
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = 'Votre Email est obligatoire.';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Votre mot de passe est obligatoire.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
