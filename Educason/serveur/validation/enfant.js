const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateEnfantInput(data) {
  let errors = {};

  data.handle = !isEmpty(data.handle) ? data.handle : '';
  data.nom = !isEmpty(data.nom) ? data.nom : '';
  data.prenom = !isEmpty(data.prenom) ? data.prenom : '';
  data.institution = !isEmpty(data.institution) ? data.institution : '';
  data.trouble = !isEmpty(data.trouble) ? data.trouble : '';

  if (!Validator.isLength(data.handle, { min: 2, max: 40 })) {
    errors.handle = 'Le handle doit être compris entre 2 et 40 caractères.';
  }

  if (Validator.isEmpty(data.handle)) {
    errors.handle = 'Le handle est obligatoire.';
  }

  if (Validator.isEmpty(data.nom)) {
    errors.nom = 'Le nom de l\'enfant est obligatoire.';
  }

  if (Validator.isEmpty(data.prenom)) {
    errors.prenom = 'Le prénom de l\'enfant est obligatoire.';
  }

  if (Validator.isEmpty(data.institution)) {
    errors.institution = 'L\'institution de l\'enfant est obligatoire.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
