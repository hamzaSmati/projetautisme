const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Chargement du modele partitions
const Note = require('../../models/Note');

// @route   GET api/Note/test
// @desc    Test de la route notes
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Note OK' }));

router.get('/getList', (req, res) => {
  Note.find()
    .then(notes => res.json(notes))
    .catch(err => res.status(404).json({ nonotesfound: 'No notes found' }));
});

// db.survey.find({ "notes.midiNumber": { $elemMatch: { midiNumber: req.params.midiNumber }}})

router.get('/get1Note/:midiNumber', (req, res) => {
  Note.find({midiNumber: req.params.midiNumber})
    .sort({ midiNumber: 1 })
    .then(notes => res.json(notes))
    .catch(err => res.status(404).json({ nonotesfound: 'No notes found' }));
});


module.exports = router;
