const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Chargement du modele partitions
const Partition = require('../../models/Partition');

// @route   GET api/partitions/test
// @desc    Test de la route partitions
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Partitions OK' }));

router.get('/getList', (req, res) => {
  Partition.find()
    .then(partitions => res.json(partitions))
    .catch(err => res.status(404).json({ nopartitionfound: 'No partition found' }));
});

router.get('/getList/name', (req, res) => {
  Partition.find({}, {name:1, _id:0})
    .sort({ date: -1 })
    .then(partitions => res.json(partitions))
    .catch(err => res.status(404).json({ nopartitionsfound: 'No partitions found' }));
});

router.get('/get1partition/:name', (req, res) => {
  Partition.findOne({name: req.params.name} )
    .sort({ date: -1 })
    .then(partitions => res.json(partitions))
    .catch(err => res.status(404).json({ nopartitionsfound: 'No partitions found' }));
});
// @route   GET api/users/register
// @desc    Enregistrement d'un ultilsateur
// @access  Public
router.post('/savePartition', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
);
    //res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    //console.log(req.body);
    const newPartiton = new Partition({
        name: req.body.name,
        notes: req.body.notes
      });

      newPartiton.save()
      .then(partition => res.json(partition))
      .catch(err => console.log(err));
    
});

router.get('/getList/name', (req, res) => {
    Partition.find({}, {name:1, _id:0})
      .sort({ date: -1 })
      .then(partitions => res.json(partitions))
      .catch(err => res.status(404).json({ nopartitionsfound: 'No partitions found' }));
  });

module.exports = router;
