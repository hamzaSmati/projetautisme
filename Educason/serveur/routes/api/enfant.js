const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Chargement des fonctions validant les inputs
const validateEnfantInput = require('../../validation/enfant');

// Chargement du modèle Enfant 
const Enfant = require('../../models/Enfant');

// Chargement du modèle Utilisateur 
const User = require('../../models/User');

// @route   GET api/enfant/test
// @desc    Test route enfant
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Route Enfant OK' }));

// @route   GET api/enfant/all
// @desc    Retourne tous les enfants associés à l'éducateur
// @access  Privée
router.get(
  '/all', 
  passport.authenticate('jwt', { session: false }), 
  (req, res) => {
  const errors = {};

  Enfant.find({ user: req.user.id })
    .populate('user', ['name', 'avatar'])
    .then(enfants => {
      if (enfants === undefined || enfants.length == 0) {
        errors.aucunenfant = 'Aucun enfant ne vous est rattaché.'; 
        return res.status(404).json(errors);
      }
      res.json(enfants);
    })
    .catch(err => res.status(404).json({ enfant: 'Aucun enfant ne vous est rattaché.' }));
});

// @route   POST api/enfant
// @desc    Creation d'un enfant
// @access  Privé
router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
      const { errors, isValid } = validateEnfantInput(req.body);
  
    if (!isValid) {
      return res.status(400).json(errors);
    }
    // Récupération des champs de l'enfant
    const champsEnfant = {};
    champsEnfant.user = req.user.id;
    if (req.body.handle) champsEnfant.handle = req.body.handle;
    if (req.body.nom) champsEnfant.nom = req.body.nom;
    if (req.body.prenom) champsEnfant.prenom = req.body.prenom;
    if (req.body.dateNaissance) champsEnfant.dateNaissance = req.body.dateNaissance;
    if (req.body.institution) champsEnfant.institution = req.body.institution;
    if (req.body.trouble) champsEnfant.trouble = req.body.trouble;
     
    // Vérifier si l'enfant existe déjà
    Enfant.findOne({ nom: req.body.nom }).then(enfant => {
        if (enfant) {
          errors.nom = 'Cette enfant existe déjà.';
          return res.status(400).json(errors);
        } else { 
          // Création d'un nouveau enfant
          // Sauvegarde de l'enfant
          new Enfant(champsEnfant).save().then(enfant => res.json(enfant));
          
          // Liaison de l'enfant à l'éducateur (tableau 'enfants' du modèle User)
          User.findOne({ _id: req.user.id }).then(user => {
            Enfant.findOne({ user: req.user.id }).sort([['_id', -1]]).then(enfant => {
              user.enfants.unshift(enfant._id);
              user.save();
            });              
          });       
        }
      });
  });

// @route   PUT api/enfant
// @desc    Modification d'un enfant
// @access  Privé
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateEnfantInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }
    
    // Récupération des champs de l'enfant
    const champsEnfant = {};
    champsEnfant.user = req.user.id;
    if (req.body.handle) champsEnfant.handle = req.body.handle;
    if (req.body.nom) champsEnfant.nom = req.body.nom;
    if (req.body.prenom) champsEnfant.prenom = req.body.prenom;
    if (req.body.dateNaissance) champsEnfant.dateNaissance = req.body.dateNaissance;
    if (req.body.institution) champsEnfant.institution = req.body.institution;
    if (req.body.trouble) champsEnfant.trouble = req.body.trouble;
       
    Enfant.findOne({ user: req.user.id }).then(enfant => { // chercher par nom de l'enfant
        if (enfant) {
          // Mis à jour de l'enfant
          Enfant.findOneAndUpdate(
            { nom: req.body.nom },
            { $set: champsEnfant },
            { new: true }
          ).then(enfant => res.json(enfant));
        }
  });

});

// @route   DELETE api/enfant/:id_enfant
// @desc    Suppression d'un enfant rattaché à un éducateur
// @access  Private
router.delete(
  '/:id_enfant',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Enfant.findOneAndRemove({ _id: req.params.id_enfant })
    .then(enfant => {res.json({ message : 'Enfant supprimé'})})
    .catch(err => res.status(404).json(err));
    
    //Suppression de l'id de l'enfant (tableau 'enfants' du modèle User)
    User.findOne({ _id: req.user.id })
    .then(user => {
      // Récupération de l'index de l'enfant à supprimer
      const removeIndex = user.enfants.indexOf(req.params.id_enfant);

      // Suppression de l'enfant du tableau 'enfants'
      user.enfants.splice(removeIndex, 1);

      // Sauvegarde de la suppression
      user.save();
    })
    .catch(err => res.status(404).json(err));
  }
);

module.exports = router;