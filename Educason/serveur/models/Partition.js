const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Création du Schéma utilisateur
const partionSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  notes:[
      {
          midiNumber: {
            type: Number
          },
          time: {
            type: Number
          },
          duration: {
            type: Number
          }
       }
],  
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Partiton = mongoose.model('Partitions', partionSchema);
