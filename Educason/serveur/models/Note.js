const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Création du Schéma utilisateur
const noteSchema = new Schema({

    midiNumber: {
        type: Number
    },
    url: {
        type: String
    }

});

module.exports = Note = mongoose.model('Note', noteSchema);
