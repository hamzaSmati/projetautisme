const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EnfantSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  handle: {
    type: String,
    required: true,
    max: 40
  },
  nom: {
    type: String,
    required: true
  },
  prenom: {
    type: String,
    required: true
  },
  dateNaissance: {
    type: Date
  },
  institution: {
    type: String,
    required: true
  },
  trouble: {
    type: String
  }
});

module.exports = Enfant = mongoose.model('enfant', EnfantSchema);
