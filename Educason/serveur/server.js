const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

const users = require('./routes/api/users');
const enfant = require('./routes/api/enfant');
const partitions = require('./routes/api/partitions');
const notes = require('./routes/api/notes');
const note = require('./routes/api/note');


const app = express();

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// BD Config
const db = require('./config/keys').mongoURI;

// Connexion à MongoDB
mongoose
  .connect(db)
  .then(() => console.log('MongoDB OK'))
  .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

// Utilisateur Routes
app.use('/api/users', users);
app.use('/api/partitions', partitions);
app.use('/api/enfant', enfant);
app.use('/api/notes', notes);
app.use('/api/note', note);


const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server OK sur le port ${port}`));
